<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Intraweb
 * @package    Intraweb_Vpos
 * @copyright  Copyright (c) 2011 Riccardo Roscilli
 */

class Intraweb_Vposcc_ProcessingController extends Mage_Core_Controller_Front_Action
{
    protected $_successBlockType = 'vposcc/success';
    protected $_failureBlockType = 'vposcc/failure';
    protected $_cancelBlockType = 'vposcc/cancel';

    protected $_order = NULL;
    protected $_paymentInst = NULL;
    protected $_paymentID = NULL;

    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * when customer selects Vpos payment method
     */
 
   public function redirectAction()
    {
        try {
            $session = $this->_getCheckout();
 			
            $order = Mage::getModel('sales/order');
            
            $order->loadByIncrementId($session->getLastRealOrderId());
            
            $this->_paymentInst = $order->getPayment()->getMethodInstance();

            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
   

            if ($session->getQuoteId() && $session->getLastSuccessQuoteId()) {
                $session->setVposccQuoteId($session->getQuoteId());
                $session->setVposccSuccessQuoteId($session->getLastSuccessQuoteId());
                $session->setVposccRealOrderId($session->getLastRealOrderId());
                $session->getQuote()->setIsActive(false)->save();
                $session->clear();
            }
            	if(!file_exists('IGFS_CG_API/init/IgfsCgInit.php')) die ("Le librerie IGFS non sono installate sul server");
		   		require('IGFS_CG_API/init/IgfsCgInit.php');
		   
				$init = new IgfsCgInit();
				if ($this->_paymentInst->getConfigData('environment') == 'Test')
				{
					$init->serverURL = $this->_paymentInst->getConfigData('server_url_test');
					$init->disableCheckSSLCert();
				}
				else 
					$init->serverURL = $this->_paymentInst->getConfigData('server_url_production');
					
				$init->timeout = $this->_paymentInst->getConfigData('timeout');
				$init->tid = $this->_paymentInst->getConfigData('terminal_id');
				$init->kSig = $this->_paymentInst->getConfigData('kSig');
				$init->shopID = $order->getincrementId();
				$init->shopUserRef = $order->getcustomerEmail();
				$init->trType = $this->_paymentInst->getConfigData('transaction_type');
				$init->currencyCode = $this->_paymentInst->getConfigData('currency_code');
			//$order->getgrandTotal()
				$init->amount = (int)number_format($order->getgrandTotal(),"2", "","");// arrotonda alla seconda cifra decimale ed esprime tutto in centesimi
				
				$init->landID = "IT"; // ??
				$init->notifyURL = Mage::getBaseUrl(). $this->_paymentInst->getConfigData('notify_url');
				$init->errorURL = Mage::getBaseUrl(). $this->_paymentInst->getConfigData('error_url');
				// User Field 1
				$init->addInfo1 = "";
				// User Field 2
				$init->addInfo2 = "";
				// User Field 3
				$init->addInfo3 = "";
				// User Field 4
				$init->addInfo4 = "";
				// User Field 5
				$init->addInfo5 = "";
			
			    if (!$init->execute()) {
				 	die(urlencode($init->rc)."&errorDesc=".urlencode($init->errorDesc));					
				header("location: error.php?rc=".urlencode($init->rc)."&errorDesc=".urlencode($init->errorDesc));
				} else {
                $order->addStatusToHistory( $this->_paymentInst->getConfigData('order_status'), Mage::helper('vposcc')->__((string)($init->paymentID)));
                $order->save();	
				header("location: ".$init->redirectURL);
				}
			    exit;
 			
            $this->loadLayout();
            $this->renderLayout();
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('Vposcc error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/onepage/success/');
    }

    /**
     * Vpos returns POST variables to this action
     */
    public function responseAction()
    {
    	
        try {
        
            $request = $this->_checkReturnedPost();
            $session = $this->_getCheckout();

			
            if ($request->rc == 'IGFS_000') {
     /* 		 $this->_order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
             $this->_order->addStatusToHistory( $this->_paymentInst->getConfigData('first_order_status'), Mage::helper('vposcc')->__((string)($init->paymentID)));
               //  $order->sendNewOrderEmail();
        	   // 	$order->setEmailSent(true);
        	   // disabilita l'invio della mail di conferma d'ordine al cliente
        	   // Mage::app()->getStore()->setConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_ENABLED, "0");
        	 $this->_order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
             $this->_order->save();	
             */
             $this->_processSale($request);// imposta l'ordine come pagato con i dati della transazione 

             $this->_order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
             $this->_order->addStatusToHistory( $this->_paymentInst->getConfigData('first_order_status'), 'Pagato Carta di credito, tranId :' . $request->tranID);
        	 $this->_order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
             $this->_order->save();	
             
             
             $this->_redirect('vposcc/processing/success/');
            
               
            } elseif ($request->rc !== 'IGFS_000') {
                   $this->_processCancel($request);

					
            } else {
            	$this->_redirect('checkout/cart');
            
            }
           
        } catch (Mage_Core_Exception $e) {
        	 $request = $this->_checkReturnedPost();

            $this->_debug('Vposcc response error: ' . $e->getMessage());
            $this->getResponse()->setBody(
                $this->getLayout()
                    ->createBlock($this->_failureBlockType)
                    ->setOrder($this->_order)
                    ->toHtml()
            );
        }
    }

    /**
     * Vpos return action
     */
    public function successAction()
    {
        try {
            $session = $this->_getCheckout();
            $session->unsVposccRealOrderId();
            $session->setQuoteId($session->getVposccQuoteId(true));
            $session->setLastSuccessQuoteId($session->getVposccSuccessQuoteId(true));
            $this->_redirect('checkout/onepage/success');
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('Vposcc error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Vpos return action
     */
    public function cancelAction()
    {
        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getVposccQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }
        $session->addError(Mage::helper('vposcc')->__('The order has been canceled.'));
        $this->_redirect('checkout/cart');
    }


    /**
     * Checking POST variables.
     * Creating invoice if payment was successfull or cancel order if payment was declined
     */
    protected function _checkReturnedPost()
    {
    	
    
        $session = $this->_getCheckout();
        $this->_order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());

        if (!$this->_order->getId())
            Mage::throwException('Order not found');
	
            $this->_paymentInst = $this->_order->getPayment()->getMethodInstance();
            
 			$orderComments = $this->_order->getAllStatusHistory();
		    $paymentID = $orderComments[0]['comment'];
            
      
        
           require('IGFS_CG_API/init/IgfsCgVerify.php');
           
				$tid = $this->_paymentInst->getConfigData('terminal_id');
				$shopID = $this->_order->getIncrementId();
				
				$verify = new IgfsCgVerify();
				if ($this->_paymentInst->getConfigData('environment') == 'Test')
				{
					$verify->serverURL = $this->_paymentInst->getConfigData('server_url_test');
					$verify->disableCheckSSLCert();
				}
				else 
					$verify->serverURL = $this->_paymentInst->getConfigData('server_url_production');
					
				$verify->timeout = $this->_paymentInst->getConfigData('timeout');
				$verify->tid = $tid;
				$verify->kSig = $this->_paymentInst->getConfigData('kSig');
				$verify->shopID = $shopID;
				$verify->paymentID = $paymentID;
				
				
				if (!$verify->execute()) {
            	   $this->_order->cancel();
            		$this->_order->save();
			         $request = $verify;
				} else {
	                 $this->_order->sendNewOrderEmail();
	        		 $this->_order->setEmailSent(true);
	        		 $this->_order->save();	
	           		         
			         $request = $verify;
				}
	
        return $request;
    }

    /**
     * Process success response
     */
    protected function _processSale($request)
    {
        // check transaction amount and currency

        // save transaction information
		$tranID = $request->tranID;
		$authCode = $request->authCode;
		$authStatus = $request->authStatus;
		$brand = $request->brand;
		
		mage::log("tranId ".$tranID);
		mage::log("authCode ".$authCode);
		mage::log("authStaus ".$authStatus);
		mage::log("brand ".$brand);

        $this->_order->getPayment()
        	->setTransactionId($tranID)
        	->setLastTransId($tranID)
        	->setIsTransactionClosed(1)
        	->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, array('TransactionID'=>$tranID,'brand'=>$brand)); 
        	//->setCcAvsStatus($authStatus)
        	//->setCcType($brand);

        // save fraud information
  
        switch($authStatus) {
            case 'Y':
            case 'U':  	
            if ($this->_order->canInvoice()) {	
                 	$order = $this->_order;
               		$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

					$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
					$invoice->register();
					// $invoice->register();
		
					//
					
					$transactionSave = Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder());
					$transactionSave->save();
						
                }

				return;
                break;
            case 'E':
                $this->_order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, $this->_paymentInst->getConfigData('order_status'),  Mage::helper('vposcc')->__('preauthorize: Customer returned successfully'));
                break;
            default:
            	$this->_processCancel($request);
            	return;
        }

        $this->_order->sendNewOrderEmail();
        $this->_order->setEmailSent(true);

        $this->_order->save();
        $this->_redirect('vposcc/processing/success/');
        exit;
        return;
     //    $this->successAction();
        
       
  		$this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock($this->_successBlockType)
                ->setOrder($this->_order)
                ->toHtml()
        );
     
    }

    /**
     * Process success response
     */
    protected function _processCancel($request)
    {
        // cancel order
   
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock($this->_cancelBlockType)
                ->setOrder($this->_order)
                ->toHtml()
        );
       
    }

    protected function _getPendingPaymentStatus()
    {
        return Mage::helper('vposcc')->getPendingPaymentStatus();
    }

    /**
     * Log debug data to file
     *
     * @param mixed $debugData
     */
    protected function _debug($debugData)
    {
        if (Mage::getStoreConfigFlag('payment/vpos_bp/debug')) {
            Mage::log($debugData, null, 'payment_vpos_bp.log', true);
        }
    }
}